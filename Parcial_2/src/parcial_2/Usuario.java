package parcial_2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Usuario {

    Usuario usuario;

    private static int Nro_Cuenta;
    private static int NIP;
    private static int Saldo = 500;

    @Override
    public String toString() {
        return "Usuario{}";
    }

    public static int getSaldo() {
        return Saldo;
    }

    public static int getNro_Cuenta() {
        return Nro_Cuenta;
    }

    public static int getNIP() {
        return NIP;
    }

    public static void setNIP(int NIP) {
        Usuario.NIP = NIP;
    }

    public static void setSaldo(int Saldo) {
        Usuario.Saldo = Saldo;
    }

    Usuario(int Nro_Cuenta, int NIP) {
        this.Nro_Cuenta = Nro_Cuenta;
        this.NIP = NIP;

    }
    ATM atm;
    Connection laConexion;
    PreparedStatement Retiro;
    PreparedStatement Deposito;
    Statement stmtConsulta;
    ResultSet rs;

    /**
     * ************Se mapean manualmente las variables de la base de datos***********************
     */
    public Usuario getUsuario(String numeroDeCuenta, String nip) {
        try {
            // Define la conexion
            laConexion = AdministradorDeConexiones.getConnection();

            // Arma la consulta y la ejecuta
            String laConsulta = "SELECT * FROM usuarios";
            stmtConsulta = laConexion.createStatement();
            rs = stmtConsulta.executeQuery(laConsulta);

            // Muestra los datos
            while (rs.next()) {
                System.out.println("Nro_Cuenta: " + rs.getString("Nro_cuenta") + " -- "
                        + "NIP: " + rs.getString("NIP")
                        + "Saldo: " + rs.getString("Saldo"));

                Usuario usuario = new Usuario(rs.getInt("Nro_cuenta"),
                        rs.getInt("NIP"));

                return usuario;
            }
            // Cierra el Statement y la Connection
            stmtConsulta.close();
            laConexion.close();
        } catch (SQLException | ReflectiveOperationException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * ***************************************************************************************************
     */
    /**
     * ********************************************Retiro*************************************************
     */
    public void retirar(int extraer) throws SQLException, ReflectiveOperationException {
        try {
            laConexion = AdministradorDeConexiones.getConnection();
            if (ConsultarSaldo(Usuario.getSaldo()) > extraer) {

                Retiro = laConexion.prepareStatement(
                        "UPDATE usuarios " + "SET Saldo = ? - '" + extraer + "'WHERE Nro_Cuenta = ?");
                Retiro.setInt(1, Usuario.getSaldo());
                Retiro.setInt(2, Usuario.getNro_Cuenta());
                Retiro.execute();
                JOptionPane.showMessageDialog(null,
                        "Retiro Exitoso \n" + "Su Nuevo Saldo es: " + ConsultarSaldo(Usuario.getSaldo()));
                System.out.println(Retiro);
            } else {
                JOptionPane.showMessageDialog(null, "Saldo Insuficiente");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        laConexion.close();
    }

    /**
     * *******************************************************************************************************
     */

    /**
     * **********************************************Deposito*************************************************
     */
    public void Deposito(int depositar) throws SQLException, ReflectiveOperationException {

        laConexion = AdministradorDeConexiones.getConnection();
        try {

            Deposito = laConexion.prepareStatement(
                    "UPDATE usuarios " + "SET Saldo = ? + '" + depositar + "'WHERE Nro_Cuenta = ?");
            Deposito.setInt(1, Usuario.getSaldo() + depositar);
            Deposito.setInt(2, Usuario.getNro_Cuenta());
            Deposito.execute();
            System.out.println(Deposito);

        } catch (SQLException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        laConexion.close();

    }

    /**
     * *******************************************************************************************************
     */

    public int ConsultarSaldo(int saldo) throws SQLException, ReflectiveOperationException {

        try {

            laConexion = AdministradorDeConexiones.getConnection();
            String ConsultaSaldo = "SELECT Saldo FROM Usuarios";
            rs = stmtConsulta.executeQuery(ConsultaSaldo);
            while (rs.next()) {
                JOptionPane.showMessageDialog(null,
                        "Su saldo es" + rs.getString("Saldo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Bienvenida.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (saldo);
    }

    public void ingresar(int Nro_Cuenta, int NIP) throws SQLException, ReflectiveOperationException {

        laConexion = AdministradorDeConexiones.getConnection();

        String ingresar = "SELECT * FROM usuarios WHERE Nro_Cuenta ='" + Nro_Cuenta + "' AND NIP =" + NIP;
        rs = stmtConsulta.executeQuery(ingresar);
        if (rs.next()) {
            JFrame bienvenida = new Bienvenida();
            bienvenida.setVisible(true);
            atm.dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Validacion Incorrecta");
        }
        laConexion.close();
    }
}
